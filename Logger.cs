namespace CustomLogger
{
    public class Logger
    {

        ConsoleColor originalColor;

        public void Log(string message, LogType type, bool newLine = true) => Log(type.ToString(), message, type, null, newLine);

        public void Log(string title, string message, LogType type, bool newLine = true) => Log(title, message, type, newLine);

        public void Log(string title, string message, LogType type, ConsoleColor? textColor, bool newLine)
        {
            // Save current text color
            originalColor = Console.ForegroundColor;

            // Define new color based on log type or on input
            Console.ForegroundColor = textColor == null ? DefineColor(type) : (ConsoleColor)textColor;

            // Write colored title
            if (type != LogType.Standard)
                Console.Write($"\n[{title}] ");

            // Set previous text color back
            Console.ForegroundColor = originalColor;

            // Write message
            Console.Write($"{message}{(newLine ? "\n" : string.Empty)}");
        }

        private ConsoleColor DefineColor(LogType type)
        {
            switch (type)
            {
                case LogType.Standard:
                    goto default;
                case LogType.Error:
                    return ConsoleColor.Red;
                case LogType.Warning:
                    return ConsoleColor.Yellow;
                case LogType.Info:
                    return ConsoleColor.Blue;
                case LogType.Debug:
                    return ConsoleColor.Green;
                default:
                    return originalColor;
            }
        }
    }
}