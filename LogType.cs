namespace CustomLogger
{
    public enum LogType
    {
        Title,
        Standard,
        Error,
        Warning,
        Info,
        Debug
    }
}