# Custom Logger

A basic logger library written in C#. It prints out text  with different colors based on the enum value provided.

It's very very simple right now, but I plan on improving it in the future.

Feel free to fork, to modify it and suggest improvements. Use it the way you want to, it's MIT licensed and you can legally do whatever you want with this code.

## License
MIT License.
